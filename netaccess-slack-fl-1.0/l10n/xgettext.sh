#!/bin/bash

### go to this dir
cd $(dirname $0)

### get appname
appname=$(./get_app_name.sh)

### get the translatable strings from the modules
find ../modules/ -name '*.php' -o -name '*.js' -o -name '*.html' \
  | xargs xgettext -C --keyword=T_ --output=$appname.po

### get the translatable strings from the php files in the app dir
xgettext -C --keyword=T_ --join-existing --output=$appname.po ../*.php

### get the translatable strings from webobjects
find ../webobjects/ -name '*.php' -o -name '*.js' -o -name '*.html' \
  | xargs xgettext -C --keyword=T_ --join-existing --output=$appname.po
