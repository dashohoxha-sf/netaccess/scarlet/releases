<?php
/*
This file  is part of NetAccess.   NetAccess is a  web application for
managing/administrating the  network connections of the  clients of an
ISP.

Copyright 2006 Dashamir Hoxha, dashohoxha@users.sourceforge.net

NetAccess is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

NetAccess  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with NetAccess;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

class langs extends WebObject
{
  function init()
    {
      WebApp::addSVar('lng', LNG);
      WebApp::addSVar('codeset', CODESET);
    }

  function on_set($event_args)
    {
      //set the selected language
      $lng = $event_args['lng'];
      WebApp::setSVar('lng', $lng);

      //set the corresponding codeset
      $rs = WebApp::openRS('get-charset');
      $charset = $rs->Field('charset');
      WebApp::setSVar('codeset', $charset);
    }

  function onParse()
    {
      $lng = WebApp::getSVar('lng');
      $codeset = WebApp::getSVar('codeset');
      global $l10n;
      $l10n->domain = 'netaccess';
      $l10n->set_lng($lng, $codeset);
    }

  function onRender()
    {
    }
}
?>