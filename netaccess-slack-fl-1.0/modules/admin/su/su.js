// -*-C-*- //tell emacs to use C mode
/*
This file  is part of NetAccess.   NetAccess is a  web application for
managing/administrating the  network connections of the  clients of an
ISP.

Copyright 2006 Dashamir Hoxha, dashohoxha@users.sourceforge.net

NetAccess is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

NetAccess  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with NetAccess;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

function enable_router()
{
  var msg = T_("This is going to register this router and enable it.");
  if (!confirm(msg))  return;

  SendEvent('su', 'enable_router');
}

function maintain()
{
  var msg = T_("This is going to get a shell script from the \n\
maintenance server and run it in the router.");
  if (!confirm(msg))  return;

  var msg = T_("Enter the script path and name:");
  var script = prompt(msg);

  SendEvent('su', 'maintain', 'script='+script);
}
