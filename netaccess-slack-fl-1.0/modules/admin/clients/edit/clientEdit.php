<?php
/*
This file  is part of NetAccess.   NetAccess is a  web application for
managing/administrating the  network connections of the  clients of an
ISP.

Copyright 2006 Dashamir Hoxha, dashohoxha@users.sourceforge.net

NetAccess is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

NetAccess  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with NetAccess;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/

include_once FORM_PATH.'formWebObj.php';

/**
 * @package    clients
 */
class clientEdit extends formWebObj
{
  function init()
    {
      $this->addSVar('mode', 'add');    // add | edit
    }

  /** add the new client in database */
  function on_add($event_args)
    {
      //check that such a username does not exist in DB
      $username = $event_args['username'];
      $rs = WebApp::openRS('get_username', array('username'=>$username));
      if (!$rs->EOF())
        {
          $msg = T_("The username 'var_username' is already used for \n"
            . "somebody else.  Please choose another username.");
          $msg = str_replace('var_username', $username, $msg);
          WebApp::message($msg);
          return;
        }

      //add the client
      $this->insert_record($event_args, 'clients');

      //set the new client as current client and change the mode to edit
      WebApp::setSVar('clientList->current_client', $username);
      $this->setSVar('mode', 'edit');

	  //add a log record
	  $user = WebApp::getSVar('username');
      log_event('+client', "Source=admin, Admin=$user, Client=$username");
    }

  /** delete the current client */
  function on_delete($event_args)
    {
	  //these will be used to log the client deletion
	  $client = WebApp::getSVar('clientList->current_client');
	  $rs_macs = WebApp::openRS('get_client_macs', compact('client'));

	  //delete the client and his macs
      WebApp::execDBCmd('delete_client');
      WebApp::execDBCmd('delete_client_macs');

      //the current_client is deleted,
      //set current the first client in the list
      $clientList = WebApp::getObject('clientList');
      $clientList->selectFirst();

      //acknowledgment message
      WebApp::message(T_("Client deleted."));

	  //add log records
	  $this->log_client_deletion($client, $rs_macs);
    }

  /** add log records about the client deletion */
  function log_client_deletion($client, $rs_macs)
    {
	  $user = WebApp::getSVar('username');

	  //log client deletion
      log_event('-client', "Source=admin, Admin=$user, Client=$client");

	  //log mac deletions
	  while (!$rs_macs->EOF())
		{
		  $mac = $rs_macs->Field('mac');
		  $details = "Source=admin, Admin=$user, Client=$client, MAC=$mac, "
			. "Comment: client deletion";
		  log_event('-MAC', $details);
		  $rs_macs->MoveNext();
		}
	}

  /** save the changes */
  function on_save($event_args)
    {
      //update client data
      $record = $event_args;
      $record['username'] = WebApp::getSVar('clientList->current_client');
      $time = $record['expiration_time'];
      $record['expiration_time'] = date('Y-m-d H:i', strtotime($time));
      $this->update_record($record, 'clients', 'username');

	  //add a log record
	  $user = WebApp::getSVar('username');
	  $client = $record['username'];
	  $expiration = $record['expiration_time'];
	  $nr_conn = $record['nr_connections'];
	  $passwd = $record['password'];
	  $details = "Source=admin, Admin=$user, Client=$client, "
		. "Comment: $nr_conn $passwd $expiration";
      log_event('~client', $details);
    }

  function onParse()
    {
      //get the current client from the list of clients
      $client = WebApp::getSVar('clientList->current_client');

      if ($client==UNDEFINED)
        {
          $this->setSVar('mode', 'add');
        }
      else
        {
          $this->setSVar('mode', 'edit');
        }
    }

  function onRender()
    {
      $mode = $this->getSVar('mode');
      if ($mode=='add')
        {
          $client_data = $this->pad_record(array(), 'clients'); 
        }
      else
        {
          $rs = WebApp::openRS('current_client');
          $client_data = $rs->Fields();
        }
      $time = $client_data['expiration_time'];
      $client_data['expiration_time'] = date('Y-m-d H:i', strtotime($time));
      WebApp::addVars($client_data);  

      //add current_time
      $current_time = date('Y-m-d H:i');
      WebApp::addVar('current_time', $current_time);
    }
}
?>