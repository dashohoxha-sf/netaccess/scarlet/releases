<?php
/*
This file  is part  of HGSM.   HGSM is a  web application  for keeping
information about a hierarchical structure (in this case a grid).

Copyright 2005, 2006 Dashamir Hoxha, dashohoxha@users.sourceforge.net

HGSM is free software; you  can redistribute it and/or modify it under
the terms of  the GNU General Public License as  published by the Free
Software  Foundation; either  version 2  of the  License, or  (at your
option) any later version.

HGSM is  distributed in the hope  that it will be  useful, but WITHOUT
ANY WARRANTY; without even  the implied warranty of MERCHANTABILITY or
FITNESS FOR A PARTICULAR PURPOSE.   See the GNU General Public License
for more details.

You  should have received  a copy  of the  GNU General  Public License
along with HGSM; if not,  write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/


/**
 * @package clients
 */
class mac_edit extends WebObject
{
  var $mac_record = array(
                           'mac'       => '',
                           'timestamp' => '',
                           'hostname'  => ''
                           );
  
  function init()
    {
      $this->addSVar('mode', 'hidden');  // add | edit | hidden
      $this->addSVar('mac', UNDEFINED);
    }

  function on_save($event_args)
    {
      $mode = $this->getSVar('mode');
      $event_args['client'] = WebApp::getSVar('clientList->current_client');
      $event_args['timestamp'] = time();

      if ($mode=='add')
        {
          $this->add_mac($event_args);
        }
      else if ($mode=='edit')
        {
          WebApp::execDBCmd('update_mac', $event_args);
          $this->setSVar('mode', 'hidden');

		  //log the event
		  $user = WebApp::getSVar('username');
		  $mac = $event_args['mac'];
		  $d = "Source=admin, Admin=$user, MAC=$mac, Comment: modification";
		  log_event('~MAC', $d);
        }

    }

  function add_mac($record)
    {
      //check that mac does not exist in the table
      $rs = WebApp::openRS('get_mac', $record);
      if (!$rs->EOF())
        {
          $this->mac_record = $record;
          $mac = $record['mac'];
          $msg = T_("MAC 'v_mac' is already used.");
          $msg = str_replace('v_mac', $mac, $msg);
          WebApp::message($msg);
          return;
        }

      //allow this MAC in the firewall
      $path = APP_PATH."server-config/firewall";
	  $mac = $record['mac'];
      shell("$path/mac-allow.sh $mac");

      //add the new mac
      WebApp::execDBCmd('add_mac', $record);

      //switch the editing mode to hidden
      $this->setSVar('mode', 'hidden');

	  //log the event
	  $user = WebApp::getSVar('username');
	  $d = "Source=admin, Admin=$user, MAC=$mac, Comment: adding";
      log_event('+MAC', $d);
    }

  function on_cancel($event_args)
    {
      //switch the editing mode to hidden
      $this->setSVar('mode', 'hidden');
    }

  function onRender()
    {
      $mode = $this->getSVar('mode');
      if ($mode=='add')
        {
          $mac = WebApp::getSVar('client->MAC');
          if ($mac != UNDEFINED)
            {
              //the webbox is used is used in the client interface
              //get the hostname and and the mac of the client computer
              $hostname = WebApp::getSVar('client->computer_name');
              $this->mac_record['mac'] = $mac;
              $this->mac_record['hostname'] = $hostname;
            }
          WebApp::addVars($this->mac_record);
        }
      else if ($mode=='edit')
        {
          $args = array('mac' => $this->getSVar('mac'));
          $rs = WebApp::openRS('get_mac', $args);
          $vars = $rs->Fields();
          WebApp::addVars($vars);
        }
    }
}
?>