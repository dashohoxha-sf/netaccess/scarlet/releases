<?php
/*
This file  is part of NetAccess.   NetAccess is a  web application for
managing/administrating the  network connections of the  clients of an
ISP.

Copyright 2006 Dashamir Hoxha, dashohoxha@users.sourceforge.net

NetAccess is free  software; you can redistribute it  and/or modify it
under the terms of the GNU  General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

NetAccess  is distributed  in the  hope that  it will  be  useful, but
WITHOUT   ANY  WARRANTY;   without  even   the  implied   warranty  of
MERCHANTABILITY  or FITNESS  FOR  A PARTICULAR  PURPOSE.  See the  GNU
General Public License for more details.

You  should have received  a copy  of the  GNU General  Public License
along with NetAccess;  if not, write to the  Free Software Foundation,
Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA
*/


include_once FORM_PATH.'formWebObj.php';

/**
 * @package settings
 */
class settings extends formWebObj
{
  function on_save($event_args)
    {
      //delete all the current settings
      WebApp::execQuery('DELETE FROM settings');

      //save the new settings
      reset($event_args);
      while (list($name,$value) = each($event_args))
        {
          $arr_values[] = "('$name', '$value')";
        }
      $values = implode(', ', $arr_values);
      $query = "INSERT INTO settings (name, value) VALUES $values";
      WebApp::execQuery($query);
    }

  /** save the values from the database to the config files and apply them */
  function on_apply($event_args)
    {
      $arr_settings = $this->get_settings();
      $settings = '';
      while (list($name,$value) = each($arr_settings))
        {
          $settings .= "$name=$value\n";
        }
      $settings .= "\nDOMAIN=localnet.net\n";
      $settings .= "ACCESS_IP=".ACCESS_IP."\n";

      //get the name of the config file
      $fname = APP_PATH.'server-config/gateway.cfg';

      //backup the config file (in any case)
      shell("cp -f $fname $fname.bak");

      //write the new settings to the config file and reconfig the server
      write_file($fname, $settings);
      shell('server-config/reconfig.sh');

	  //add a log record
	  $user = WebApp::getSVar('username');
	  $details = "Source=admin, Admin=$user, Action=apply, Comment: $settings";
      log_event('~settings', $details);
    }

  function on_get_current_values($event_args)
    {
      $this->on_save($this->get_current_settings());
    }

  function on_get_default_values($event_args)
    {
      $arr_settings = array('WAN_IP'         => '192.168.1.2/24',
                            'GATEWAY'        => '192.168.1.1',
                            'DNS'            => '192.168.1.1',
                            'LAN_IP'         => '192.168.0.1/24',
                            'UPLOAD_LIMIT'   => '80',
                            'DOWNLOAD_LIMIT' => '386'
                            );
      $this->on_save($arr_settings);
    }

  /** test the new settings for a short time */
  function on_test($event_args)
    {
      $time = $event_args['time'];
      $arr_settings = $this->get_settings();
      $settings = '';
      while (list($name,$value) = each($arr_settings))
        {
          $settings .= "$name=$value\n";
        }
      $settings .= "\nDOMAIN=localnet.net\n";
      $settings .= "ACCESS_IP=".ACCESS_IP."\n";

      $fname = APP_PATH.'server-config/gateway.cfg.test';
      write_file($fname, $settings);
      shell("server-config/test.sh $time");

	  //add a log record
	  $user = WebApp::getSVar('username');
	  $details = "Source=admin, Admin=$user, Action=test, "
		. "Comment: time=$time $settings";
      log_event('~settings', $details);
    }

  function onRender()
    {
      WebApp::addVars($this->get_settings());

      //current_settings
      $arr_settings = $this->get_current_settings();
      while (list($name, $value) = each($arr_settings) )
        {
          $arr_curr_settings['CURRENT_'.$name] = $value;
        }
      WebApp::addVars($arr_curr_settings);
    }

  /** returns an associative array with the settings and their values */
  function get_settings()
    {
      $arr_settings = array();
      $rs = WebApp::execQuery('SELECT * FROM settings');
      while (!$rs->EOF())
        {
          $name = $rs->Field('name');
          $value = $rs->Field('value');
          $arr_settings[$name] = $value;
          $rs->MoveNext();
        }

      return $arr_settings;
    }

  /** returns an associative array with the current settings */
  function get_current_settings()
    {
      $arr_settings = array();
      $arr_lines = file(APP_PATH."server-config/gateway.cfg");
      for ($i=0; $i < sizeof($arr_lines); $i++)
        {
          $line = trim($arr_lines[$i]);
          if ($line=='')  continue;
          if ($line[0]=='#') continue;
          list($name,$value) = split('=', $line, 2);
          $arr_settings[$name] = $value;
        }

      return $arr_settings;
    }
}
?>
