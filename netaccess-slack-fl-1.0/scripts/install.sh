#!/bin/bash

### go to the application dir
cd $(dirname $0)
cd ..

### make executable all the script files
find . -name '*.sh' | xargs chmod +x

### compile wrapper.c and set proper ownership/permissions
gcc -o wrapper wrapper.c
chown root:root wrapper
chmod +s wrapper

### create the database
db/create.sh

### compile the translation files
langs="en sq_AL nl it de"
for lng in $langs
do
  l10n/msgfmt.sh $lng
done

### create the script that runs 'check.php' hourly
file=/etc/cron.hourly/netaccess.sh
cat <<EOF > $file
#!/bin/bash
$app_path/check.php
EOF
chmod +x $file

