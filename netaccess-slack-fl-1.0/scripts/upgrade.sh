#!/bin/bash
### This script is usually executed by the button 'Upgrade'
### in the admin interface. It usually gets the latest release.

if [ "$1" = "" ]
then
  echo "Usage: $0 release-tag"
  exit 1
fi

release_tag=$1

### go to the app dir
cd $(dirname $0)
cd ..

### get the svn url of the new release
url=$(svn info | grep 'URL:' | gawk '{print $2}')
url=${url%/branches*}
url=${url%/releases*}
url=$url/releases/$release_tag

### switch to the new release
svn switch $url

### run the maintenance script to fix anything that needs to be fixed
scripts/maintain.sh

