#!/bin/bash
### reconfigure the network after changing
### any configuration variables

### go to this directory
cd $(dirname $0)

### configure network interfaces
./network.sh

### configure iptables (firewall, port forwarding, SNAT, etc.)
./firewall/iptables.sh

### restart dnsmasq
/etc/rc.d/rc.dnsmasq restart

### restart httpd
/etc/rc.d/rc.httpd restart

### trafic control
trafic/trafic-change.sh
