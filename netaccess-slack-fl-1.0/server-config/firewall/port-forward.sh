#!/bin/bash
### enables or disables port forwardings 

function usage
{
  echo "Usage: $0 PORT EXT_IP INT_IP SERVER_IP [enable|disable|status]
    PORT       is the port that will be forwarded (like 80 or http)
    EXT_IP     is the external IP of the server
    INT_IP     is the IP of through which it will be forwarded
    SERVER_IP  is the IP of the server to which it will be forwarded"
  exit
}

### check that there are at least 4 parameters
if [ $# -lt 4 ]; then usage; fi

### get the parameters and remove any netmask from IP-s
PORT=$1
EXT_IP=${2%/*}
INT_IP=${3%/*}
SERVER_IP=${4%/*}
ACTION=$5


### append or delete the forward rules according to the parameter
function iptables-fwd-rules
{
  command=$1
  IPT=/usr/sbin/iptables

  # accept the port for forwarding (both tcp and udp)
  $IPT --table filter --$command FORWARD \
       --match state --state NEW \
       --match tcp --protocol tcp --dport $PORT \
       --jump ACCEPT
  $IPT --table filter --$command FORWARD \
       --match udp --protocol udp --dport $PORT \
       --jump ACCEPT

  ### forward the port
  $IPT --table nat --$command PREROUTING \
       --protocol tcp --destination $EXT_IP --dport $PORT \
       --jump DNAT --to-destination $SERVER_IP
  $IPT --table nat --$command PREROUTING \
       --protocol udp --destination $EXT_IP --dport $PORT \
       --jump DNAT --to-destination $SERVER_IP

  ### in case that $INT_IP is not gateway for $SERVER_IP
  $IPT --table nat --$command POSTROUTING \
       --protocol tcp --destination $SERVER_IP --dport $PORT \
       --jump SNAT --to-source $INT_IP
}

case $ACTION in
  enable  )  iptables-fwd-rules append ;;
  disable )  iptables-fwd-rules delete ;;
  *       )  /usr/sbin/iptables-save | grep $SERVER_IP | grep "dport $PORT" ;;
esac

