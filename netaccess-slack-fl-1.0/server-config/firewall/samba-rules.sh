#!/bin/bash
### The rules of the chain SAMBA.
### This chain is used to accept the samba ports.

### accept the udp ports 137 and 138
APPEND='/usr/sbin/iptables --table filter --append SAMBA'
$APPEND --match udp --protocol udp --dport 137 --jump ACCEPT
$APPEND --match udp --protocol udp --dport 138 --jump ACCEPT

### accept the tcp ports 139 and 445
./port.sh 139 accept SAMBA
./port.sh 445 accept SAMBA

### return to the previous chain
$APPEND --jump RETURN

