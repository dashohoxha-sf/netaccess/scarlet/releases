#!/bin/bash
# enable or disable source NAT (masquerading)

function usage
{
  echo "Usage: ${0} [enable|disable|list] [interface] [external_ip]"
  exit
}

### check that there is one parameter
if [ -z $1 ]; then usage; fi

ACTION=$1
OUT_IF=${2:-eth0}
EXTERNAL_IP=$3

if [ -z $EXTERNAL_IP ]
then
  TARGET=MASQUERADE
else
  TARGET="SNAT --to-source $EXTERNAL_IP"
fi

### enable forwarding in the kernel
echo 1 > /proc/sys/net/ipv4/ip_forward

### append or delete the SNAT rules
function iptables-snat-rule
{
  command=$1
  IPT=/usr/sbin/iptables

  $IPT --table nat --$command POSTROUTING \
       --out-interface $OUT_IF \
       --jump $TARGET
}

### list the existing SNAT rules 
function list-snat-rules
{
  /usr/sbin/iptables-save --table nat \
      | grep -E 'SNAT|MASQUERADE' \
      | grep --invert-match 'dport' 
}

case $ACTION in
  enable  )  iptables-snat-rule append ;;
  disable )  iptables-snat-rule delete ;;
  *       )  list-snat-rules ;;
esac

