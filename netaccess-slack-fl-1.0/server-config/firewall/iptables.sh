#!/bin/bash
### this script configures the firewall

### go to this directory
cd $(dirname $0)
### include network configuration variables
. ../gateway.cfg

IPT=/usr/sbin/iptables

### clear all the tables
$IPT --table filter --flush
$IPT --table filter --delete-chain
$IPT --table nat    --flush
$IPT --table mangle --flush

### accept by default anything in the OUTPUT chain 
$IPT --table filter --policy OUTPUT ACCEPT

### forward ports
./forward-ports.sh

### redirect some ports (like 53, 80, etc.) to localhost
./redirect.sh

### enable SNAT-ing
EXT_IP=${WAN_IP%/*}
./source-nat.sh enable eth0 $EXT_IP

### the rules of the INPUT chain
./input-rules.sh

### the rules of the FORWARD chain
./forward-rules.sh

### if there is any argument, display the rules
if [ $# != 0 ]
then
  /usr/sbin/iptables-save
fi
