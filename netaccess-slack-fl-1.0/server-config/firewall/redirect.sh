#!/bin/bash
### redirect some ports (like 53, 80, etc.) to localhost

IPT='/usr/sbin/iptables --table nat'
APPEND="$IPT --append PREROUTING"

### redirect any DNS requests (port 53) to the localhost
$APPEND --protocol udp --destination-port 53 --jump REDIRECT
$APPEND --protocol tcp --destination-port 53 --jump REDIRECT

### accept the HTTP requests (port 80) of the allowed MAC-s
for MAC in $(cat '../allowed-macs')
do
  $APPEND --protocol tcp --destination-port 80 \
          --match mac --mac-source $MAC --jump RETURN
done

### redirect HTTP requests (port 80) of the others to the localhost
$APPEND --protocol tcp --destination-port 80 --jump REDIRECT

