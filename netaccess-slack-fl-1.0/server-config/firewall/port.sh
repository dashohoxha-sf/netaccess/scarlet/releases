#!/bin/bash
### Accept (or deny, or show status of) tcp connections in the given port
### by appending or deleting a rule in the given chain.

function usage
{
  echo "Usage: $0 [port] [accept|deny|status] [chain] [proto]
    where port is a number (like 80) or a service name (like http)
    chain is: INPUT, FORWARD, LOCALNET, WAN, etc.
    and proto is: tcp or udp or tcp-udp (default is tcp)"
  exit
}

if [ $# = 0 ]; then usage; fi

PORT=$1
ACTION=$2
CHAIN=${3:-INPUT}
PROTO=${4:-tcp}

## appends or deletes a rule, according to the parameter
function iptables-rule
{
  command=$1
  IPT=/usr/sbin/iptables

  if [ $PROTO = "tcp" -o $PROTO = "tcp-udp" ]
  then
    $IPT --table filter --$command $CHAIN \
         --match state --state NEW \
         --match tcp --protocol tcp --destination-port $PORT \
         --jump ACCEPT
  fi

  if [ $PROTO = "udp" -o $PROTO = "tcp-udp" ]
  then
    $IPT --table filter --$command $CHAIN \
         --match udp --protocol udp --destination-port $PORT \
         --jump ACCEPT
  fi
}

case $ACTION in
  accept )  iptables-rule append ;;
  deny   )  iptables-rule delete ;;
  *      )  /usr/sbin/iptables-save | grep $CHAIN | grep "dport $PORT" ;;
esac

