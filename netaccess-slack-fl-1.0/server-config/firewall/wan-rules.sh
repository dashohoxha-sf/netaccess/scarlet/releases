#!/bin/bash
### The rules of the chain WAN, which are applied to input from eth0,
### which is connected to the WAN network.

IPT='/usr/sbin/iptables --table filter'
APPEND="$IPT --append WAN"

### accept httpd (80, 8080)
./port.sh 80 accept WAN
./port.sh 8080 accept WAN

### accept ssh (port 2222)
. ../gateway.cfg
ACCESS_IP=${ACCESS_IP%/*}
$APPEND --match state --state NEW --destination $ACCESS_IP \
        --match tcp --protocol tcp --destination-port 2222 --jump ACCEPT

### return to the previous chain
#$APPEND --jump RETURN
#
# This is commented because the default is to return to the previous chain.
# Also, if it is commented, the script 'port.sh' can be used to
# accept or reject any other ports (by appending or deleting rules).
# If it is uncommented, then any other rules that are added later 
# will come after this one, and since this one matches anything, they 
# will not be reached. The order of the rules does matter!

