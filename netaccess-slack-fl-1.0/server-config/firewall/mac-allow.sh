#!/bin/bash
### Allow the given MAC to connect to internet.

if [ $# = 0 ]
then
  echo "Usage: $0 mac-address"
  exit
fi

MAC=$1

IPT="/usr/sbin/iptables"

### allow connections from this MAC to be forwarded
# delete the last (reject all) rule from the chain
$IPT --table filter --delete FORWARD \
     --jump REJECT --reject-with icmp-net-unreachable
# append another rule into the chain to allow this MAC
$IPT --table filter --append FORWARD \
     --match mac --mac-source $MAC --jump ALLOWED-MACS
# append the last (reject all) rule into the chain
$IPT --table filter --append FORWARD \
     --jump REJECT --reject-with icmp-net-unreachable

### allow the HTTP requests (port 80) from this MAC
# delete the last (reject all) rule from the chain
$IPT --table nat --delete PREROUTING \
     --protocol tcp --destination-port 80 --jump REDIRECT
# append another rule into the chain to allow this MAC
$IPT --table nat --append PREROUTING \
     --protocol tcp --destination-port 80 \
     --match mac --mac-source $MAC --jump RETURN
# append the last (reject all) rule into the chain
$IPT --table nat --append PREROUTING \
     --protocol tcp --destination-port 80 --jump REDIRECT

