#!/bin/bash
### Deny the given MAC connection to internet.

if [ $# = 0 ]
then
  echo "Usage: $0 mac-address"
  exit
fi

MAC=$1

IPT="/usr/sbin/iptables"

### deny connections from this MAC to be forwarded
# delete from the chain the rule that allows this MAC
$IPT --table filter --delete FORWARD \
     --match mac --mac-source $MAC --jump ALLOWED-MACS

### deny the HTTP requests (port 80) from this MAC
# delete from the chain the rule that allows this MAC
$IPT --table nat --delete PREROUTING \
     --protocol tcp --destination-port 80 \
     --match mac --mac-source $MAC --jump RETURN

