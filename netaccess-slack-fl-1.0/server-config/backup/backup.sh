#!/bin/bash

### go to this directory
cd $(dirname $0)

### collect config files
tar --create --gzip --files-from=config-files.txt --file=config-files.tgz

