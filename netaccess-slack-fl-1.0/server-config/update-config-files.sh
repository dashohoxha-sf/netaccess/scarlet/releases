#!/bin/bash
### modify the configuration files after changing
### any configuration variables

### go to this directory
cd $(dirname $0)

### include the configuration file
. gateway.cfg

### change the resolv.conf files
echo "nameserver $DNS" > /etc/resolv.conf
echo "nameserver $DNS" > /etc/resolv.dnsmasq.conf

### update /etc/hosts
LOCAL_IP=${LAN_IP%/*}
sed -e "s/|DOMAIN|/$DOMAIN/g" \
    -e "s/|LOCAL_IP|/$LOCAL_IP/g" \
    templates/hosts > /etc/hosts

### change the configuration of dnsmasq
LAN_NETWORK=${LAN_IP%.*/*}
sed -e "s/|DOMAIN|/$DOMAIN/g"  \
    -e "s/|LAN_NETWORK|/$LAN_NETWORK/g"        \
    templates/dnsmasq.conf > /etc/dnsmasq.conf

### change the configuration of HTTPD
APP_PATH=$(dirname $(pwd))
sed -e "s#|APP_PATH|#$APP_PATH#g"  \
    templates/httpd.conf > /etc/apache/httpd.conf
cp templates/mod_php.conf /etc/apache/mod_php.conf

### upload and download rate limits
mv -f trafic/trafic.cfg trafic/trafic.oldcfg
cat <<EOF > trafic/trafic.cfg
UPLOAD=$UPLOAD_LIMIT
DOWNLOAD=$DOWNLOAD_LIMIT
EOF

