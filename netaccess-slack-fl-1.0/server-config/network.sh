#!/bin/bash
### reconfigure the network after changing any parameters

### go to this dir
cd $(dirname $0)

### include the configuration file
. ./gateway.cfg

### set up the links, in  case that they are not up
/sbin/ip link set eth0 up
/sbin/ip link set eth1 up

### flush any existing ip addresses of eth0 and eth1
/sbin/ip address flush eth0
/sbin/ip address flush eth1

### add new addresses; eth0 is connected to the WAN, the others to the LAN-s
/sbin/ip address add $WAN_IP dev eth0
/sbin/ip address add $LAN_IP dev eth1

### add the access IP to the external (WAN, eth0) interface
/sbin/ip address add $ACCESS_IP dev eth0

### fix some routes
#/sbin/ip route del $NET.0/24 dev eth0
#/sbin/ip route add $GATEWAY dev eth0

### add a default route
add_route="/sbin/ip route add"
$add_route to default via $GATEWAY

