#!/bin/bash
### reconfigure the network after changing
### any configuration variables

### go to this directory
cd $(dirname $0)

### update the configuration files
./update-config-files.sh

### configure the server
./configure.sh

