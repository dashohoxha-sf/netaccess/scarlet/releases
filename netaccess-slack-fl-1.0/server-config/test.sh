#!/bin/bash
### This script is for testing any new configurations.
### When the server is accessed and managed remotely,
### something may go wrong and it is possible to lock
### yourself out. To avoid such troubles, this script
### tests any new configs and after a certain time
### (e.g. 60 secs) it goes back to the old (tried and true)
### configuration. During the sleep time you can test
### the new configuration and make sure that it is OK.

if [ "$1" = "" ]
then
  echo "Usage: $0 time"
  echo "where 'time' is the test time in seconds"
  exit
fi

time=$1

### go to this directory
cd $(dirname $0)

### backup the current config file
cp gateway.cfg gateway.cfg.bak

### use the new config file
cp -f gateway.cfg.test gateway.cfg

### reconfig the server using the new config file
./reconfig.sh

### wait for the given nr of seconds
sleep $time

### copy back the config file
cp -f gateway.cfg.bak gateway.cfg

### reconfig the server using the old config file
./reconfig.sh

