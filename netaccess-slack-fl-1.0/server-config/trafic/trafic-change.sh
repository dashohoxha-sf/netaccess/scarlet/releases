#!/bin/bash
### update the upload and download limits

### go to this dir
cd $(dirname $0)

### turn off the old values
. ./trafic.oldcfg
./download-rate-limit.sh off $DOWNLOAD
./upload-rate-limit.sh off $UPLOAD

### turn on the new values
. ./trafic.cfg
./download-rate-limit.sh on $DOWNLOAD
./upload-rate-limit.sh on $UPLOAD
