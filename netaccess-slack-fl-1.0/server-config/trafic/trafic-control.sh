#!/bin/bash

function usage
{
  echo "Usage: $0 [on|off] [dev] [rate]"
  echo "  dev   can be eth0, eth1, etc."
  echo "  rate  can be 48, 96, etc."
  exit
}

if [ $# = 0 ]; then usage; fi
ACTION=$1
DEV=${2:-eth1}
RATE=${3:-220}

## Change 220kbit to your uplink's *actual* speed, minus a few percent.xi
## If you have a really fast modem, raise 'burst' a bit.

case $ACTION in
  on )
    /sbin/tc qdisc add dev $DEV root tbf rate ${RATE}kbit latency 50ms burst 1540
    ;;
  off )
    /sbin/tc qdisc del dev $DEV root tbf rate ${RATE}kbit latency 50ms burst 1540
    ;;
  * )
    usage
    ;;
esac

